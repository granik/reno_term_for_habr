#ifndef MAINWINDOW_H
#define MAINWINDOW_H
#include <QPlainTextEdit>
#include <QMainWindow>
#include <QComboBox>
#include <QtCore/QDateTime>
#include <QtGui/QScrollBar>
#include <qwt_plot.h>
#include <QLayout>
#include <qwidget.h>
#include <QPushButton>
#include <serialdeviceenumerator.h>
#include <abstractserial.h>

namespace Ui {
    class MainWindow;
}


class Plot;

class MainWindow : public QMainWindow
{
    Q_OBJECT
signals:

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
protected:
    void changeEvent(QEvent *e);

private slots:
    //
    void procEnumerate(const QStringList &l);
    void slotPrintAllDevices(const QStringList &list);
    void procSerialMessages(const QString &msg, QDateTime dt);
    void procSerialDataReceive();
    void printTrace(const QByteArray &data);
    void RecToFile(QPointF point);

    // Proc buttons click
    void procControlButtonClick();
    void about();

private:

    Ui::MainWindow *ui;
    SerialDeviceEnumerator *enumerator;
    AbstractSerial *serial;

    void initMainWidgetCloseState();
    void initMainWidgetOpenState();

    void initEnumerator();
    void deinitEnumerator();
    void initSerial();
    void deinitSerial();
    void initButtonConnections();
    void initBoxConnections();

    void createActions();
    void createMenus();
    void createToolBars();
    void createCentralWidget();

    QMenu *helpMenu;
    QPlainTextEdit *textEdit;
    QAction *aboutqtAct;
    QAction *aboutAct;
    QAction *controlButton;
    QComboBox *portBox;
    Plot *plot;
    QGridLayout *layout;
    QByteArray dataArray;

};
#endif // MAINWINDOW_H
