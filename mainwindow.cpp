#include <QtCore/QStringList>
#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QMessageBox>
#include "plot.h"



MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    enumerator(0),
    serial(0),
    dataArray(0)
{
    ui->setupUi(this);
    createActions();
    createMenus();
    createToolBars();
    createCentralWidget();

    this->initEnumerator();
    this->initSerial();
    this->initButtonConnections();
    this->initMainWidgetCloseState();

}

MainWindow::~MainWindow()
{
    this->deinitEnumerator();
    this->deinitSerial();
    delete ui;
}

void MainWindow::createActions()
{
     aboutAct = new QAction(tr("About"), this);
     aboutAct->setStatusTip(tr("Show the About box"));
     connect(aboutAct, SIGNAL(triggered()), this, SLOT(about()));


     aboutqtAct = new QAction(tr("AboutQT"), this);
     aboutqtAct->setStatusTip(tr("Show the Qt library's About box"));
     connect(aboutqtAct, SIGNAL(triggered()), qApp, SLOT(aboutQt()));

     controlButton = new QAction(tr("Open"), this);
     controlButton->setStatusTip(tr("Open port"));

 }

void MainWindow::createMenus()
 {
     helpMenu = menuBar()->addMenu(tr("&Help"));
     helpMenu->addAction(aboutqtAct);
     helpMenu->addAction(aboutAct);
 }

void MainWindow::createCentralWidget()
 {
    textEdit = new QPlainTextEdit;
    textEdit->setSizePolicy(QSizePolicy::Expanding,
          QSizePolicy::Fixed);

    plot = new Plot(this);

    layout = new QGridLayout();
    layout->addWidget(plot, 0, 0);
    layout->addWidget(textEdit,1,0);
    centralWidget()->setLayout(layout);

    setWindowTitle(tr("QSerialDevice and QWT for HabraHabr"));
 }

void MainWindow::createToolBars()
 {
     portBox = new QComboBox(ui->tb);
     portBox->setObjectName("Ports");
     ui->tb->addWidget(portBox);
     ui->tb->addAction(controlButton);
 }

void MainWindow::about()
 {
    QMessageBox::about(this, tr("About Application"),
             tr("<b>QSerialDevice and QWT for HabraHabr</b> - example of simple terminal<p> Created by Alex Alexeev. 2011"));
 }

void MainWindow::changeEvent(QEvent *e)
{
    QWidget::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        ui->retranslateUi(this);
        break;
    default:
        break;
    }
}

/* Private slots section */

void MainWindow::procEnumerate(const QStringList &l)
{
    // Fill ports box.
    portBox->clear();
    portBox->addItems(l);
}

void MainWindow::procSerialMessages(const QString &msg, QDateTime dt)
{
    QString s = dt.time().toString() + " > " + msg;
    textEdit->appendPlainText(s);
}

void MainWindow::procSerialDataReceive()
{
    if (this->serial && this->serial->isOpen())
    {
        QByteArray byte = this->serial->readAll();
        this->printTrace(byte);

        if(byte.at(0)!='\n')
        {
            dataArray.append(byte);
        }
        else
        {
            if(dataArray.at(0)=='C')
            {
                if(dataArray.at(3) == '3')
                {
                   double elapsed = (plot -> dclock_elapsed())/ 1000.0;
                   QByteArray u;
                   //u[0]= dataArray.at(5);
                   int j=5;
                   /*for(j=5;j<9;j++)
                   {
                        if(dataArray.at(j)!='\r') u[j-5]= dataArray.at(j);
                   }*/
                   while(dataArray.at(j)!='\r')
                   {
                       u[j-5]= dataArray.at(j);
                       j++;
                   }
                   QPointF point(elapsed,u.toDouble()*5/1024);
                   plot ->appendPoint(point);
                   RecToFile(point);
                }
            }

            dataArray = 0;
         }
    }

}

void MainWindow::RecToFile(QPointF point)
{
    QFile f("test.dat");
    if (f.open(QIODevice::Append | QIODevice::Text))
    {
        QTextStream out(&f);
        out << point.x() << "\t" << point.y() << "\n";
        f.close();
    }
    else
    {
        qWarning("Can not open file test.dat");
    }
}

void MainWindow::printTrace(const QByteArray &data)
{
    //textEdit->setTextColor((directionRx) ? Qt::darkBlue : Qt::darkGreen);
    textEdit->insertPlainText(QString(data));

    QScrollBar *bar = textEdit->verticalScrollBar();
    bar->setValue(bar->maximum());
}

void MainWindow::procControlButtonClick()
{
    if (this->serial)
    {
        bool result = this->serial->isOpen();
        if (result)
        {
            this->serial->close();
            result = false;
        }
        else
        {
            this->serial->setDeviceName(portBox->currentText());
            result = this->serial->open(QIODevice::ReadWrite);

            if (!result)
            {
                       qDebug() << "Serial device by default: " << this->serial->deviceName() << " open fail.";
                       return;
            }

                   //Дефолтные параметры соединения
                   qDebug() << "= Default parameters =";
                   qDebug() << "Device name            : " << this->serial->deviceName();
                   qDebug() << "Baud rate              : " << this->serial->baudRate();
                   qDebug() << "Data bits              : " << this->serial->dataBits();
                   qDebug() << "Parity                 : " << this->serial->parity();
                   qDebug() << "Stop bits              : " << this->serial->stopBits();
                   qDebug() << "Flow                   : " << this->serial->flowControl();
                   qDebug() << "Total read timeout constant, msec : " << this->serial->totalReadConstantTimeout();
                   qDebug() << "Char interval timeout, usec       : " << this->serial->charIntervalTimeout();

                   //Теперь можно установить свои параметры, посмотрим списки возможных значений параметров:
                   qDebug() << "List of possible baudrates : " << this->serial->listBaudRate();

                   qDebug() << "List of possible flowcontrols : " << this->serial->listFlowControl();

                   //Например установим следующие параметры:
                   if (!this->serial->setBaudRate(AbstractSerial::BaudRate9600)) {
                       qDebug() << "Set baud rate " <<  AbstractSerial::BaudRate9600 << " error.";
                       return;
                   };

                   if (!this->serial->setDataBits(AbstractSerial::DataBits8)) {
                       qDebug() << "Set data bits " <<  AbstractSerial::DataBits8 << " error.";
                       return;
                   }

                   if (!this->serial->setParity(AbstractSerial::ParityNone)) {
                       qDebug() << "Set parity " <<  AbstractSerial::ParityNone << " error.";
                       return;
                   }

                   if (!this->serial->setStopBits(AbstractSerial::StopBits1)) {
                       qDebug() << "Set stop bits " <<  AbstractSerial::StopBits1 << " error.";
                       return;
                   }

                   if (!this->serial->setFlowControl(AbstractSerial::FlowControlOff)) {
                       qDebug() << "Set flow " <<  AbstractSerial::FlowControlOff << " error.";
                       return;
                   }

            plot->start();

        }

        (result) ? this->initMainWidgetOpenState() : this->initMainWidgetCloseState();
    }
}

void MainWindow::slotPrintAllDevices(const QStringList &list)
{
    qDebug() << "\n ===> All devices: " << list; //выводим список портов
    //textEdit->appendPlainText("\n ===> All devices:\n ");
    foreach (QString s, list)
    {
        //textEdit->appendPlainText("\n" + s);
        this->enumerator->setDeviceName(s); //устанавливает имя текущего устройства,
        //теперь можно использовать методы класса для извлечения сведений об устройстве
        qDebug() << "\n <<< info about: " << this->enumerator->name() << " >>>";
        //textEdit->appendPlainText("\n <<< info about: " + this->enumerator->name() + " >>>");

        qDebug() << "-> description  : " << this->enumerator->description();
        //textEdit->appendPlainText("-> description  : " + QString(this->enumerator->description()));

        qDebug() << "-> is busy      : " << this->enumerator->isBusy();
        //textEdit->appendPlainText("-> is busy: " + QString(this->enumerator->isBusy()));

    }
}

/* Private methods section */

void MainWindow::initMainWidgetCloseState()
{
    portBox->setEnabled(true);
    controlButton->setText(QString(tr("Open")));
}

void MainWindow::initMainWidgetOpenState()
{
    portBox->setEnabled(false);
    controlButton->setText(QString(tr("Close")));
}

void MainWindow::initEnumerator()
{
    if (this->enumerator)
        return;
    this->enumerator = new SerialDeviceEnumerator(this);
    connect(this->enumerator, SIGNAL(hasChanged(QStringList)), this, SLOT(procEnumerate(QStringList)));
    connect(this->enumerator, SIGNAL(hasChanged(QStringList)), this, SLOT(slotPrintAllDevices(QStringList)));
    this->enumerator->setEnabled(true);
}

void MainWindow::deinitEnumerator()
{
    if (this->enumerator && this->enumerator->isEnabled())
        this->enumerator->setEnabled(false);
}

void MainWindow::initSerial()
{
    if (this->serial)
        return;
    this->serial = new AbstractSerial(this);
    connect(this->serial, SIGNAL(signalStatus(QString,QDateTime)), this, SLOT(procSerialMessages(QString,QDateTime)));
    connect(this->serial, SIGNAL(readyRead()), this, SLOT(procSerialDataReceive()));

    // Enable emmiting signal signalStatus
    this->serial->enableEmitStatus(true);
}

void MainWindow::deinitSerial()
{
    if (this->serial && this->serial->isOpen())
        this->serial->close();
}

void MainWindow::initButtonConnections()
{
    connect(controlButton, SIGNAL(triggered()), this, SLOT(procControlButtonClick()));
}


