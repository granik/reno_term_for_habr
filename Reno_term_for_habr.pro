#-------------------------------------------------
#
# Project created by QtCreator 2011-05-21T17:24:33
#
#-------------------------------------------------

QT       += core gui

TARGET = Reno_term_for_habr
TEMPLATE = app

CONFIG += static
CONFIG += qwt

include($${PWD}/../src/qserialdeviceenumerator/qserialdeviceenumerator.pri)
include($${PWD}/../src/qserialdevice/qserialdevice.pri)


#INCLUDEPATH += /home/granik/qserialdevice-qserialdevice-0.4.0/qserialdevice-qserialdevice/src/qserialdevice
#INCLUDEPATH += /home/granik/qserialdevice-qserialdevice-0.4.0/qserialdevice-qserialdevice/src/qserialdeviceenumerator #$${PWD}/../src

#QMAKE_LIBDIR += /home/granik/qserialdevice-qserialdevice-0.4.0/BuildLibrary0/src/build/release
#LIBS += -lqserialdevice

SOURCES += main.cpp\
        mainwindow.cpp \
    plot.cpp

HEADERS  += mainwindow.h \
    plot.h

FORMS    += mainwindow.ui \

win32 {
    LIBS += -lsetupapi -luuid -ladvapi32
}
unix:!macx {
    LIBS += -ludev
}

OTHER_FILES +=



